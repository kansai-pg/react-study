import { ReactNode } from "react"

export const Count = ({count, children}:{count:number, children:ReactNode}) => {
    return (
        <div>
            <p> {children}:{count} </p>
        </div>
    )
}
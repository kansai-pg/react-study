import React, { useState } from 'react';
import { Count } from './count';

function countupbyclick(setcount:React.Dispatch<React.SetStateAction<number>>,count:number){
  setcount(count + 1);
  console.log(count);
}

function App() {
  const [count,setCount] = useState(0);
  return (
    <div className="App">
      <Count count={count}><strong>demokin</strong></Count>
      <button onClick={() => countupbyclick(setCount, count)}> demokin </button>

    </div>
  );
}

export default App;
